import pytest

from mud_monster.basics_cog import BasicsCog


class MockBot:
    def __init__(self) -> None:
        self.user = "USER"


class MockContext:
    def __init__(self) -> None:
        self.author = "MockAuthor"
        self.sent_message = ""

    async def send(self, msg):
        self.sent_message = msg

    def get_sent_message(self):
        return self.sent_message


class MockBasicsCog(BasicsCog):
    def __init__(self, bot):
        super().__init__(bot)
        self.last_logged_message = ""

    def log_message(self, message):
        self.last_logged_message = message

    def get_last_logged_message(self):
        return self.last_logged_message


@pytest.mark.asyncio
async def test_on_ready():
    bot = MockBot()
    cog = MockBasicsCog(bot)
    await cog.on_ready()
    assert cog.get_last_logged_message() == cog.get_log_in_message()


def test_logging(capsys):
    cog = BasicsCog(None)
    msg = "This is a test message."
    cog.log_message(msg)
    captured = capsys.readouterr()
    assert captured.out == msg + "\n"


@pytest.mark.asyncio
async def test_hello():
    cog = BasicsCog(None)
    ctx = MockContext()
    msg = cog.get_hello_message(ctx)
    await cog.hello(cog, ctx)
    assert ctx.get_sent_message() == msg
