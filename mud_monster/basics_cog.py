from discord.ext import commands


class BasicsCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        self.log_message(self.get_log_in_message())

    def get_log_in_message(self):
        return "We have logged in as {0.user}".format(self.bot)

    def log_message(self, message):
        print(message)

    @commands.command()
    async def hello(self, ctx):
        await ctx.send(self.get_hello_message(ctx))

    def get_hello_message(self, ctx):
        return "Hello to you, {0}!".format(ctx.author)
